<?php
/*
Template Name: Contact us
*/
get_header(); ?>

<section class="page-hero position-relative contact-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><span class="movingletters">Contact</span></h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="container">
            <div class="row">
                <div class="col-md-4 pt-5 pb-5 contact-clipped-bg">
                    <div class="pl-3 pr-3 pb-5 pb-xl-0">
                        <h2 class="text-white text-uppercase mb-3 mb-md-4 text-center text-md-left sec-heading mt-4 animate__animated fadeup">Get in touch</span></h2>    
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
                        </div>
                        <ul class="socialinks d-flex align-items-center list-unstyled mt-4 mb-4 mb-xl-0 cnt-social animate__animated fadeup">
                            <li class="mr-3">
                                <a href="https://www.facebook.com/qldcoastal/" target="_blank">
                                    <span class="iconify" data-icon="bx:bxl-facebook" data-inline="false"></span>
                                </a>
                            </li>
                            <li class="mr-3">
                                <a href="https://www.instagram.com/qld_coastal_plumbing/" target="_blank">
                                    <span class="iconify" data-icon="bx:bxl-instagram" data-inline="false"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://au.linkedin.com/in/andrew-papotto-a0194181" target="_blank">
                                    <span class="iconify" data-icon="bx:bxl-linkedin" data-inline="false"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8 pt-5 pb-5 pl-md-5">
                    <h2 class="textDark text-uppercase mb-3 mb-md-4 text-center text-md-left sec-heading mt-4 animate__animated fadeup">For all your commercial plumbing,<br/><span class="color-sky">connect with us today!</span></h2>
                    <?= do_shortcode('[gravityform id="1" title="false" description="false" ajax="true"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="google-map mt-5 overflow-hidden">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3519.4528566768863!2d153.41657771568075!3d-28.1022278506272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b910318fd9dd773%3A0x89273b257165291e!2sQLD%20Coastal%20Plumbing!5e0!3m2!1sen!2sin!4v1618383215064!5m2!1sen!2sin" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
<?php get_footer(); ?>    