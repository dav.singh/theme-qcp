<?php
/*
Template Name: News
*/
get_header(); ?>
<section class="page-hero position-relative news-hero">
        <div class="container first">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mt-5">
                    <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><span class="movingletters">News</span></h1>
                </div>
            </div>
        </div>
        <div class="container">
            <?php
                $args = array(
                    'post_type'     => 'post',
                    'post_status'   => 'publish',
                    'orderby'       => 'ID',
                    'order'         => 'desc'                 
                );
                query_posts($args);
                $j = 1;
                while(have_posts()):the_post();
                $newsimg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'news-thumb');
                if($j == 1):
                    $shown = get_the_ID();
            ?>
            <div class="row">
                <div class="col">
                    <div class="feat-post-box bg-white p-4">
                        <div class="row align-items-center">
                            <div class="col-md-6 mb-4 mb-md-0">
                                <img src="<?= $newsimg[0]; ?>" class="img-fluid animate__animated fadeup" alt=""/>
                            </div>
                            <div class="col-md-6">
                                <div class="featbox-body pl-md-4 pr-md-5">
                                    <h2 class="textDark mb-0 animate__animated fadeup"><?= get_the_title(); ?></h2>
                                    <p class="textLight body-font mb-4 animate__animated fadeup">by <?php the_author(); ?> | <?php the_category(); ?></p>
                                    <p class="textLight body-font mt-4 mb-4 animate__animated fadeup"><?= wp_trim_words(get_the_content(),40,'...'); ?></p>
                                    <a href="<?= get_permalink(); ?>" class="btn btn-primary text-uppercase animate__animated fadeup">Read more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; $j++; endwhile; ?>
            <div class="row mt-4 mt-md-5 pb-4 pb-md-5">
                <?php 
                    while(have_posts()):the_post();
                    $newsimg = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'news-thumb');
                    if(get_the_ID() != $shown):
                ?>
                <div class="col-md-6 col-lg-4 mb-4">
                    <div class="cardbox animate__animated">
                        <div class="cardThumb">
                            <img src="<?= $newsimg[0]; ?>" class="w-100 img-fluid"/>
                        </div>
                        <div class="cardBody p-4 bg-white keepsameheight">
                            <h3 class="textDark mb-3 text-capitalize cardHeading"><?= get_the_title(); ?></h3>
                            <p class="body-font textLight"><?= wp_trim_words(get_the_content(),20,'...'); ?></p>
                            <a href="<?= get_permalink(); ?>" class="blog-link text-uppercase color-sky body-font font-bold">Read more</a>
                        </div>
                    </div>
                </div>
                <?php endif; endwhile; ?>
            </div>
        </div>
    </section>
<?php get_footer(); ?>    