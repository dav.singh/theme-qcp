<?php
/*
Template Name: QCP
*/
get_header(); ?>

<section class="hero qcp position-relative">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h1 class="text-uppercase text-white mb-4"><span class="movingletters">Commercial Pumbing</span><br/><span class="movingletters">Gold Coast</span></h1>
                    <p class="text-white mb-4 animate__animated fadeup">Providing well-managed, cost-effective and timely commercial and
                        industrial Hydraulic services to suit your project and schedule.</p>
                    <a href="#" class="btn btn-primary animate__animated fadeup">
                        <div class="d-flex align-items-center">
                            <span class="text-uppercase mr-3">Learn more</span>
                            <div class="iconfontsize">
                                <span class="iconify" data-icon="carbon:chevron-right" data-inline="false"></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="qcp-title sec-padding position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6  mb-4 mb-md-0">
                    <p class="textDark font-family-body font-bold font-size-24 text-center text-md-left animate__animated animate__bounceInLeft">For comprehensive commercial plumbing, call the experts at
                        Queensland Coastal Plumbing today!</p>
                </div>
                <div class="col-md-6 mb-4 mb-md-0">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid d-block d-md-none animate__animated fadeup" />
                </div>
            </div>
        </div>
        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid qcp-reading-floating d-none d-md-block animate__animated animate__bounceInLeft" />
    </section>
    <section class="what-we-do sec-padding no-bg-image">
        <section class="upper-blue-cut position-relative">
            <div class="container">
                <div class="row frontlayer">
                  <div class="col-md-6 offset-md-6">
                    <h2 class="textDark headingfont sec-heading mb-4 text-uppercase "><span class="movingletters">Our Commercial</span><br/><span class="movingletters">Plumbing Experience</span></h2>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">When looking for a commercial plumber, there are many factors to consider. At Queensland Coastal Plumbing, we’re a modest bunch, but we’re also proud of the work we do.</p>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">With multiple complex project sucessfully completed, it’s no wonder our reputation as the best commercial plumbers on the Gold Coast proceeds us!</p>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">With experienced staff, and state-of-the-art diagnostic equipment, our commercial plumbers are ready to handle anything. When it comes to commercial plumbing, issues can arise at any time. That’s why it’s important to choose an experienced plumber for the job.</p>
                  </div>
                </div>
              </div>
        </section>
        <section class="diagonal psudeo-diagonal sec-padding">
          <div class="container">
            <div class="row">        
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="cardbox animate__animated">
                        <div class="cardThumb">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/hydraulic.jpg" class="w-100 img-fluid"/>
                        </div>
                        <div class="cardBody p-3 bg-white keepsameheight">
                            <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                              <a href="<?= get_permalink(106); ?>">Commercial & Industrial Hydraulic Services</a>
                            </h3>
                            <p class="body-font textLight text-center">Providing well-managed, cost-effective and timely commercial and industrial plumbing to...</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="cardbox animate__animated">
                        <div class="cardThumb">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/multi-residentials.jpg" class="w-100 img-fluid"/>
                        </div>
                        <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(110); ?>">Multi-Residential Developments</a>
                          </h3>
                            <p class="body-font textLight text-center">All unit developers can expect experienced and personal, minimum-fuss...</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="cardbox animate__animated">
                        <div class="cardThumb">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/estimating.jpg" class="w-100 img-fluid"/>
                        </div>
                        <div class="cardBody p-3 bg-white keepsameheight">
                            <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                              <a href="<?= get_permalink(113); ?>">Estimating</a>
                            </h3>
                            <p class="body-font textLight text-center">QCP can help you save time and money, with easy and fast estimating and cost planning for a plumbing or drainage job.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <div class="cardbox animate__animated">
                        <div class="cardThumb">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/ohs.jpg" class="w-100 img-fluid"/>
                        </div>
                        <div class="cardBody p-3 bg-white keepsameheight">
                            <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                              <a href="<?= get_permalink(115); ?>">OH & S</a>
                            </h3>
                            <p class="body-font textLight text-center">QCP is committed to achieving the highest possible level in Occupational Health and Safety.</p>
                        </div>
                    </div>
                </div>
              </div>
          </div>
        </section>
        <section class="position-relative building-full">
            <div class="container">
                <div class="row frontlayer">
                    <div class="col-md-6 mb-4">
                      <h2 class="textDark headingfont sec-heading mb-4 text-uppercase">Commercial plumbing
                          generally encompasses a
                          wide range of applications.</h2>
                      <p class="body-font textLight font-size-smallest animate__animated fadeup">No two commercial plumbing jobs are the same, so it’s important to hire plumbers that can provide a range of services to adapt to any situation.</p>
                      <p class="body-font textLight font-size-smallest animate__animated fadeup">Here at Queensland Coastal Plumbing, we’ve seen it all and we’ve solved it all. From complex multi residential to cost-effective maintenance jobs, our extensive services can help you complete your job on time!</p>
                      <p class="body-font textLight font-size-smallest animate__animated fadeup">Nothing beats having that one-call convenience. For comprehensive commercial plumbing, call the experts at Queensland Coastal Plumbing today!</p>
                      <a href="<?= get_permalink(16); ?>" class="btn btn-primary text-uppercase mt-4 animate__animated fadeup">Book a service</a>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/building-full.jpg" class="img-fluid d-block d-md-none animate__animated fadeup"/>          
                    </div>
                  </div>
              </div>
              <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/building-full.jpg" class="floating-building d-none d-md-block animate__animated animate__bounceInRight"/>
        </section>
      </section>
      <section class="our-partners sec-padding">
          <div class="container">
              <div class="row">
                  <div class="col">
                      <h2 class="sec-heading color-sky text-uppercase text-center mb-4 mb-md-5"><span class="movingletters">Our construction partners</span></h2>
                  </div>
              </div>
              <div class="row">
                  <div class="col">
                      <div class="partner-slider">
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-1.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-2.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-3.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-4.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-5.png" alt="" /></div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked</span></h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provide services to local residents and commercial properties from South Brisbane, throughout the Gold Coasts to Tweed South and surrounding areas.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my plumber on the Gold Coast?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      No call out fees and competitive rates
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                    With our “no call out fee” service guarantee you only pay for time engaged on your job, saving money for local residents all along the Gold Coast.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul class="pl-0 list-unstyled">
                        <li><b><u>P</u></b>rofessional and progressive advice</li>
                        <li><b><u>L</u></b>egislative compliance</li>
                        <li><b><u>U</u></b>nwavering adherence to quality and service</li>
                        <li><b><u>M</u></b>arket leading rates</li>
                        <li><b><u>B</u></b>ig coverage and serviceability</li>
                        <li><b><u>I</u></b>nnovative solutions saving you time and money</li>
                        <li><b><u>N</u></b>o harm safety policy</li>
                        <li><b><u>G</u></b>old class solutions every time</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<?php get_footer(); ?>      