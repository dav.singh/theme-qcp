// requestAnimationFrame

const raf = 
  window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  function( callback ) {
    window.setTimeout( callback, 1000 / 60 )
  };

// The checker for el to be in viewport
const gambitGalleryIsInView = el => {
  const scroll = window.scrollY || window.pageYOffset
  const boundsTop = el.getBoundingClientRect().top + scroll
  const viewport = {
    top: scroll,
    bottom: scroll + window.innerHeight,
  }
  const bounds = {
    top: boundsTop,
    bottom: boundsTop + el.clientHeight,
  }
  return ( bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom ) || ( bounds.top <= viewport.bottom && bounds.top >= viewport.top );
}

// Usage to check element if in viewport
document.addEventListener( 'DOMContentLoaded', () => {
  const scrollingel = document.getElementsByClassName( 'show-on-scroll' );
  const handler = () => raf( () => {
    for(var z = 0; z < scrollingel.length; z++) 
    {
      var topull = scrollingel[z];
      if(gambitGalleryIsInView(topull))
      {
        topull.classList.add('is-visible');
      }
    }
  })
  handler()
  window.addEventListener( 'scroll', handler )
});


// Usage to check if moving letter element is in viewport
document.addEventListener( 'DOMContentLoaded', () => {
  const scrollingel = document.getElementsByClassName( 'movingletters' );
  const handler = () => raf( () => {
    for(var z = 0; z < scrollingel.length; z++) 
    {
      var topull = scrollingel[z];
      if(gambitGalleryIsInView(topull) && !topull.classList.contains("pulled"))
      {
        var elm = topull.getElementsByClassName('letter');
        anime.timeline()
        .add({
          targets: elm,
          translateY: ["1.1em", 0],
          translateZ: 0,
          duration: 750,
          delay: (el, i) => 50 * i
        });

        topull.classList.add('pulled');
      }
    }

    //check cardbox and add class to flip
    document.querySelectorAll('.cardbox').forEach(item => {
      //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
      if(gambitGalleryIsInView(item))
      {
        item.classList.add('animate__flipInX');
      }
    });

    //check fadeup element in view and add class
    document.querySelectorAll('.fadeup').forEach(item => {
      //item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
      if(gambitGalleryIsInView(item) && !item.classList.contains('animate__fadeInUp'))
      {
        item.classList.add('animate__fadeInUp');
      }
    });

  })
  handler()
  window.addEventListener( 'scroll', handler )
});


// Wrap every letter in a span for moving letters
document.querySelectorAll('.movingletters').forEach(item => {
  item.innerHTML = item.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
});




equalheight = function(container)
    {

        var currentTallest = 0,
             currentRowStart = 0,
             rowDivs = new Array(),
             $el,
             topPosition = 0;
         $(container).each(function() {
        
           $el = $(this);
           $($el).height('auto')
           topPostion = $el.position().top;
        
           if (currentRowStart != topPostion) {
             for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
               rowDivs[currentDiv].height(currentTallest);
             }
             rowDivs.length = 0; // empty the array
             currentRowStart = topPostion;
             currentTallest = $el.height();
             rowDivs.push($el);
           } else {
             rowDivs.push($el);
             currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
          }
           for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
             rowDivs[currentDiv].height(currentTallest);
           }
         });
    }

$(document).ready(function(){
    
    if($(window).width() >= 992)
    {
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
            if (scroll >= 150) {
                $("header").addClass("solid");
            }
            else{
                $("header").removeClass("solid");
            }
        });
    }    

    $('.partner-slider').slick({
        dots: false,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,      
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 1
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });

    // if($(window).width() >= 768)
    //   equalheight('.keepsameheight');


    //read more append

    var maxLength = 150;
    $(".cardBody > div").each(function(){
        var myStr = $(this).text();
        if($.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength);
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length);
            $(this).empty().html(newStr);
            $(this).append(' <a href="javascript:void(0);" class="read-more-2">read more...</a>');
            $(this).append('<span class="more-text">' + removedStr + '</span>');
        }
    });
    $(".read-more-2").click(function(){
        $(this).siblings(".more-text").contents().unwrap();
        $(this).remove();
    });

    //read more append ends
    $(".phone-link").hover(function(){
      $(this).find('.phone-icon-span').addClass('animate__tada');
      }, function(){
        $(this).find('.phone-icon-span').removeClass('animate__tada');
    });

});

$(window).resize(function(){
  if($(window).width() >= 768)
    equalheight('.keepsameheight');
});

$(window).load(function(){
  if($(window).width() >= 768)
    equalheight('.keepsameheight');
});