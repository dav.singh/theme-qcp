<?php 
/*
Template Name: OHS
*/
get_header(); ?>
<section class="page-hero position-relative services-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-9 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><span class="movingletters">OH & S</span></h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="costbar pt-4 pb-4 position-relative">
            <div class="container">
                <div class="row align-items-center text-center text-md-left">
                    <div class="col-md-8">
                        <h3 class="body-font animate__animated fadeup">Providing well-managed, cost-effective and timely commercial and industrial Hydraulic services to suit your project and schedule.</h3>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
<div class="container">
            <div class="sec-padding pb-3">
                <div class="row">
                    <div class="col-md-12 mb-4 mb-md-0">
                      <img src="<?= wp_get_attachment_url(117); ?>" alt="" class="img-fluid animate__animated fadeup" />
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-10 offset-md-1">
                        <div class="textLight body-font font-size-smallest mb-md-4">
                            <p class="animate__animated fadeup">QLD Coastal Plumbing is dedicated to providing a safe and secure workplace for all. We guarantee that our workplace activities do not jeopardize the health and safety of our workers or stakeholders.</p>
                            <p class="animate__animated fadeup">As a basic requirement, management complies with legal acts and regulations and aims to ensure a high degree of health and safety in the workplace.</p>
                            <p class="animate__animated fadeup"><strong>Consistent with these objectives:</strong></p>
                            <ol>
                                <li class="animate__animated fadeup">Safety Management System is constantly being improved.</li>
                                <li class="animate__animated fadeup">Provides safe working conditions.</li>
                                <li class="animate__animated fadeup">Ensures that the plants and equipment are in good condition.</li>
                                <li class="animate__animated fadeup">Abides with all relevant laws and regulations, especially the Western Australian Occupational Safety and Health Act and Regulations 1984, 1996.</li>
                                <li class="animate__animated fadeup">Consults with employers and other stakeholders to enhance decision-making in the areas of workplace health, welfare, and the environment.</li>
                                <li class="animate__animated fadeup">All staff receive continuing instruction and training. </li>
                                <li class="animate__animated fadeup">Maintains a safe environment for visitors and other non-employees.</li>
                                <li class="animate__animated fadeup">Any potentially dangerous circumstances, working conditions, accidents, or casualties are reported.</li>
                                <li class="animate__animated fadeup">Distributes occupational health, safety, and environment information (including this policy) to all employees and interested parties.</li>
                            </ol>
                            <p class="animate__animated fadeup">QLD Coastal Plumbing is working to establish a safety culture as a core value, in which every individual understands their duties and is motivated to collaborate as part of a team to achieve a common goal of eliminating all occupational injuries.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="our-partners sec-padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="sec-heading color-sky text-uppercase text-center mb-4 mb-md-5"><span class="movingletters">Our construction partners</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="partner-slider">
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-1.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-2.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-3.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-4.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-5.png" alt="" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked</span></h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provide services to local residents and commercial properties from South Brisbane, throughout the Gold Coasts to Tweed South and surrounding areas.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my plumber on the Gold Coast?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      No call out fees and competitive rates
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                    With our “no call out fee” service guarantee you only pay for time engaged on your job, saving money for local residents all along the Gold Coast.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul class="pl-0 list-unstyled">
                        <li><b><u>P</u></b>rofessional and progressive advice</li>
                        <li><b><u>L</u></b>egislative compliance</li>
                        <li><b><u>U</u></b>nwavering adherence to quality and service</li>
                        <li><b><u>M</u></b>arket leading rates</li>
                        <li><b><u>B</u></b>ig coverage and serviceability</li>
                        <li><b><u>I</u></b>nnovative solutions saving you time and money</li>
                        <li><b><u>N</u></b>o harm safety policy</li>
                        <li><b><u>G</u></b>old class solutions every time</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>