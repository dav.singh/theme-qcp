<?php get_header(); while(have_posts()):the_post(); ?>
<section class="page-hero position-relative services-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><?= get_the_title(); ?></h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="costbar pt-4 pb-4 position-relative">
            <div class="container">
                <div class="row align-items-center text-center text-md-left">
                    <div class="col-md-8">
                        <h3 class="body-font animate__animated fadeup animate__fadeInUp">By: <?php the_author(); ?></h3>
                        <p class="text-white body-font font-size-smallest mb-0">Posted in: <?php the_category(); ?></p>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="pt-4 pb-4 pt-md-5 pb-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= get_the_post_thumbnail( get_the_ID(), 'full', array( 'class' => 'img-fluid mx-auto mb-4 d-table' ) ); ?>
            </div>
            <div class="col-md-12 body-font font-size-regular body-font font-size-smallest">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php endwhile; get_footer(); ?>