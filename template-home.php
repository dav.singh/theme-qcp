<?php
/*
Template Name: Home
*/
get_header(); ?>
    <section class="hero position-relative">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-5">
                    <h1 class="text-uppercase text-white animate__animated fadeup">
                      Residential Plumbers<br/>
                      Gold Coast Queensland
                    </h1>
                    <p class="text-white mb-4"><span class="movingletters">All residential</span><br/><span class="movingletters">plumbing solutions</span></p>
                    <a href="<?= get_permalink(12); ?>" class="btn btn-hero no-hover">
                        <div class="d-flex align-items-center">
                            <span class="text-uppercase mr-3">Learn more</span>
                            <div class="iconfontsize">
                                <span class="iconify" data-icon="carbon:chevron-right" data-inline="false"></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="titlebar pt-5 pb-5 d-none d-lg-block">
            <div class="container-fluid">
                <div class="row">
                    <div class="offset-xl-7 col-xl-5 col-lg-6 offset-lg-6">
                        <p class="mb-0 animate__animated animate__bounceInRight">We offer residential & commercial plumbing services on the Gold coast, Burleigh & Robina.</p>
                    </div>
                </div>
            </div>
            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/who-we-are-thumb-3.png" class="floating-we-are-thumb animate__animated animate__bounceInLeft" />
        </div>
    </section>
    <div class="titlebar pt-3 pb-4 d-block d-lg-none">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="mb-0">We offer residential & commercial plumbing<br/>services on the Gold coast, Burleigh & Robina.</p>
                </div>
            </div>
        </div>
    </div>
    <section class="who-we-are sec-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 mb-4">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/who-we-are-thumb-3.png" class="img-fluid d-block d-lg-none" />
                </div>
                <div class="col-lg-5">
                    <h2 class="text-uppercase mb-4 textDark sec-heading text-center text-lg-left">Who are we</h2>
                    <p class="textLight body-font">QLD Coastal Plumbing is a full-service residential and maintenance plumbing company located in Burleigh Heads on the Gold Coast.</p>
                    <p class="textLight body-font">Our attention to detail and professionalism set us apart and provide clients with the peace of mind that any job will be completed efficiently.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="qcp-team sec-padding pt-lg-5 pb-0">
      <div class="container">
          <div class="row mt-4">        
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="cardbox animate__animated">
                    <div class="cardThumb">
                        <img src="<?= wp_get_attachment_url(205988); ?>" class="w-100 img-fluid"/>
                    </div>
                    <div class="cardBody p-4 bg-white">
                        <h3 class="textDark mb-0 text-center text-capitalize cardHeading">Andrew Papotto</h3>
                        <p class="body-font mb-0 textLight text-center">Managing Director</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4 mb-md-0">
              <div class="cardbox animate__animated">
                  <div class="cardThumb">
                      <img src="<?= wp_get_attachment_url(205989); ?>" class="w-100 img-fluid"/>
                  </div>
                  <div class="cardBody p-4 bg-white">
                      <h3 class="textDark mb-0 text-center text-capitalize cardHeading">Cliff Marchmont</h3>
                      <p class="body-font mb-0 textLight text-center">Construction Manager</p>
                  </div>
              </div>
            </div>
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="cardbox animate__animated">
                    <div class="cardThumb">
                        <img src="<?= wp_get_attachment_url(205990); ?>" class="w-100 img-fluid"/>
                    </div>
                    <div class="cardBody p-4 bg-white">
                        <h3 class="textDark mb-0 text-center text-capitalize cardHeading">
                        Rick Pope
                        </h3>
                        <p class="body-font mb-0 textLight text-center">Senior Estimator</p>
                    </div>
                </div>
            </div>
          </div>
      </div>
    </section>
    <section class="what-we-do sec-padding">
        <div class="container">
          <div class="row">
            <div class="col">
              <h2 class="headingfont textDark text-uppercase mb-4 text-center sec-heading mb-md-5">What we do</h2>
            </div>            
          </div>
          <div class="row">        
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="cardbox animate__animated">
                    <div class="cardThumb">
                      <a href="<?= get_permalink(70); ?>">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/maintenance.jpg" class="w-100 img-fluid mh-232"/>
                      </a>
                    </div>
                    <div class="cardBody p-4 bg-white">
                        <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                          <a href="<?= get_permalink(70); ?>">Residential<br/> Maintenance Plumbing</a>
                        </h3>
                        <p class="body-font textLight text-center">We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="cardbox animate__animated">
                    <div class="cardThumb">
                      <a href="<?= get_permalink(131); ?>">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/emergency.jpg" class="w-100 img-fluid mh-232"/>
                      </a>
                    </div>
                    <div class="cardBody p-4 bg-white">
                        <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                          <a href="<?= get_permalink(131); ?>">Emergency & After<br/>hours services</a>
                        </h3>
                        <p class="body-font textLight text-center">We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mb-4 mb-md-0">
                <div class="cardbox animate__animated">
                    <div class="cardThumb">
                      <a href="<?= get_site_url(); ?>/commercial-plumbing/">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/commercial-construction.jpg" class="w-100 img-fluid mh-232"/>
                      </a>
                    </div>
                    <div class="cardBody p-4 bg-white">
                        <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                          <a href="<?= get_site_url(); ?>/commercial-plumbing/">Commercial<br/>construction</a>
                        </h3>
                        <p class="body-font textLight text-center">We strive to provide complete solutions to any plumbing problems in order to make the process as easy and hassle-free as possible</p>
                    </div>
                </div>
            </div>
          </div>
          <div class="row pt-5 pb-5 frontlayer">
            <div class="col-md-6 offset-md-6">
              <h2 class="text-white headingfont sec-heading mb-4 text-uppercase">Complete plumbing<br/>solutions</h2>
              <p class="body-font text-white">Our years of experience in the commercial sector has prepared us at QLD Coastal Plumbing to handle any residential or maintenance plumbing projects. From plumbing emergencies to remodelling to new builds, our goal is to complete each project quickly and to ensure minimal disruption and a speedy return to normalcy.</p>
              <p class="body-font text-white">Our newly-formed Maintenance Service Division strives to deliver an old-fashioned plumbing service. Our residential plumbers will be on time, well-presented, and communicate with our clients throughout the job.</p>
              <p class="body-font text-white">We promise to deliver stellar service to our residential customers at a fair price. We use the latest technology to ensure our customers receive the best service.</p>
              <p class="body-font text-white">QLD Coastal Plumbing promises an experienced and professional quality service at every turn. We take a zero-tolerance approach to safety risks that sets us apart as an industry leader in safe work practices.</p>
              <p class="body-font text-white">Our consistent, speedy service allows you to rely on us whenever you need plumbing assistance. We promise a high quality finish every time. Whether you have a clogged drain or a hot water system or water issue, we're here to help you.</p>
              <a href="<?= get_permalink(16); ?>" class="btn btn-primary text-uppercase mt-4">Book a service</a>
            </div>
          </div>
        </div>
        <section class="diagonal">
          <div class="container">
            <div class="row">
              <div class="col-md-3 col-lg-3 text-white text-center text-md-left mb-4 mb-md-0 underline">
                <h3 class="text-white headingfont text-uppercase nocalloutfee"><span class="movingletters">No Call</span><br/><span class="movingletters">Out Fee</span></h3>
              </div>
              <div class="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <div class="animate__animated fadeup">
                  <span class="iconify" data-icon="cil:check-circle" data-inline="false"></span>
                  <p class="checkTitle mt-4 text-uppercase">Local plumbers in Gold Coast</p>
                </div>
              </div>
              <div class="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <div class="animate__animated fadeup">
                  <span class="iconify" data-icon="cil:check-circle" data-inline="false"></span>
                  <p class="checkTitle mt-4 text-uppercase">Fixed price no upfront cost</p>
                </div>
              </div>
              <div class="col-md-3 col-lg-3 text-white text-center mb-4 mb-md-0">
                <div class="animate__animated fadeup">
                  <span class="iconify" data-icon="cil:check-circle" data-inline="false"></span>
                  <p class="checkTitle mt-4 text-uppercase">24 hour service</p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <div class="container">
          <div class="row pt-5 pb-5 frontlayer">
              <div class="col-md-6">
                <h2 class="text-white headingfont sec-heading mb-4 text-uppercase">Emergency plumbing<br/>sevices</h2>
                <p class="body-font text-white">Our team employs local tradespeople to provide professional plumbing services to homeowners throughout the Gold Coast, so we have a plumber near you.</p>
                <p class="body-font text-white">Few things are more frustrating than a home plumbing emergency, so we are here to provide assistance in a few simple ways:</p>
                <ul class="body-font text-white">
                  <li>No call-out fees - we will only charge from the time we arrive on the job.</li>
                  <li>24/7 availability - our Rapid Response team is available around the clock for emergency plumbing services throughout the Gold Coast.</li> 
                  <li>Remote triage - our technicians will triage your plumbing problem when you call. This helps prevent further damage while our residential plumbers are en-route.</li>
                  <li>Preventative upkeep - we also provide preventative upkeep of your plumbing system to ensure your system is working properly at all times.</li>
                </ul>
                <p class="body-font text-white">Are you in need of an emergency plumber? Call us now at <a href="tel:1800 367 727">1800 367 727</a> and our emergency Rapid Response team is available 24/7 to offer assistance.</p>
                <p class="body-font text-white">Call us today for all your residential plumbing or maintenance needs!</p>
                <a href="<?= get_permalink(131); ?>" class="btn btn-primary text-uppercase mt-4">Book a service</a>
              </div>
            </div>
        </div>
      </section>
      <section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading">Get in touch</h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading">Frequently Asked</h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provides maintenance and repair services and more to residents from South Brisbane, throughout the Gold Coast to Tweed South and surrounding areas. 
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my QLD plumber?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <ul>
                        <li>Professional and prompt service</li>
                        <li>No call-out fees </li>
                        <li>Competitive rates </li>
                        <li>Stellar service</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      We have a “no call-out fee” service which guarantees you only pay for the time we’re on the job. 
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul class="">
                        <li>Blocked drains repair</li>
                        <li>Hot water heater repair and servicing</li>
                        <li>Leak detection, burst pipes, and water leaks</li>
                        <li>Gas fitting</li>
                        <li>Backflow protection and pipe repairs </li>
                        <li>Yearly preventative maintenance</li>
                        <li>Installation of toilets, taps, and other plumbing parts</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>