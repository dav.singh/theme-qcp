<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title><?php wp_title(); ?></title>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/assets/sass/main.css">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/assets/slick.css">
    <link rel="stylesheet" href="<?= get_stylesheet_directory_uri(); ?>/assets/slick-theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    
    <header class="site-header fixed-top transparent">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="<?= get_site_url(); ?>">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo-qcp-h.png" class="img-fluid" alt="" />
                </a>
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" onClick={this.toggleNavMenu}>
                <div class="ham">
                    <span class="iconify" data-icon="radix-icons:hamburger-menu" data-inline="false"></span>
                </div>
                <div class="clo">
                    <span class="iconify" data-icon="vaadin:close-big" data-inline="false"></span>
                </div>                            
                </button>
            
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <?php
                        wp_nav_menu( 
                            array(
                                'menu' => 'main_menu',
                                'depth' => 3,
                                'container' => false,
                                'menu_class' => 'navbar-nav ml-auto align-items-center',
                                'walker' => new WP_Bootstrap_Navwalker()
                            )
                        );
                    ?>
                </div>
            </nav>
        </div>
    </header>