<?php get_header(); while(have_posts()):the_post(); ?>
<section class="page-hero position-relative services-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><?php the_title(); ?></h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="costbar pt-4 pb-4 position-relative">
            <div class="container">
                <div class="row align-items-center text-center text-md-left">
                    <div class="col-md-8">
                        <h3 class="body-font animate__animated fadeup"><?= get_field('service_slogan'); ?></h3>
                        <p class="font-tiny mb-0 animate__animated fadeup">*Conditions apply</p>
                    </div>
                    <div class="col-md-4">
                    <?php $nocost = array(96,92,77); if(!in_array(get_the_ID(),$nocost)): ?>
                        <h3 class="body-font animate__animated fadeup">from</h3>
                        <h5 class="animate__animated fadeup">$<?= get_field('service_price'); ?></h5>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="sec-padding pb-3">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h2 class="textDark text-uppercase mb-3 mb-md-4 text-center sec-heading animate__animated fadeup"><?= get_field('service_intro_title'); ?></h2>
                        <div class="textLight body-font font-size-smallest mb-md-4 text-center animate__animated fadeup">
                            <?= get_field('service_intro_description'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(have_rows('service_checks')): ?>
    <section class="">
        <div class="container">
            <div class="row mt-4 mb-4">
                <?php while(have_rows('service_checks')):the_row(); ?>
                <div class="col-md-4 mb-4">
                    <div class="row align-items-center animate__animated fadeup">
                        <div class="col-3 text-center text-md-left">
                            <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color:#00c6ff;" data-width="60"></span>
                        </div>
                        <div class="col-9 pl-md-4">
                            <p class="body-font font-bold font-size-regular mb-0 "><?= get_sub_field('service_check_title'); ?></p>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if(have_rows('service_inclusions')): ?>
    <section class="sec-padding pt-3">
        <div class="container">
            <div class="row">
                <?php while(have_rows('service_inclusions')):the_row(); ?>
                    <?php if(get_sub_field('is_two_column') == 'Yes'): ?>
                        <div class="col-md-6 mb-4 mb-md-5">
                            <div class="row">
                                <div class="col-md-12">
                                    <img src="<?= wp_get_attachment_image_src(get_sub_field('service_inclusion_image'),'news-thumb')[0]; ?>" class="w-100 img-fluid"/>
                                </div>
                                <div class="col-md-12">
                                    <div class="servicebar p-4 pl-md-5 pr-md-5 keepsameheight">
                                        <h3 class="textDark mb-3 text-capitalize cardHeading animate__animated fadeup">
                                            <?= get_sub_field('service_inclusion_title'); ?>
                                        </h3>
                                        <div class="body-font textLight animate__animated fadeup">
                                            <?= get_sub_field('service_inclusion_desc'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="col-md-12 mb-4 mb-md-5">
                            <div class="row">
                                <div class="col-md-4 pr-md-0">
                                    <div class="serv-thumb h-100" style="background-image: url(<?= wp_get_attachment_image_src(get_sub_field('service_inclusion_image'),'news-thumb')[0]; ?>);background-repeat: no-repeat;background-position:center;background-size:cover">
                                        <img src="<?= wp_get_attachment_image_src(get_sub_field('service_inclusion_image'),'news-thumb')[0]; ?>" class="w-100 img-fluid invisible"/>
                                    </div>
                                </div>
                                <div class="col-md-8 pl-md-0">
                                    <div class="servicebar p-4 pl-md-5 pr-md-5 h-100">
                                        <h3 class="textDark mb-3 text-capitalize cardHeading animate__animated fadeup">
                                            <?= get_sub_field('service_inclusion_title'); ?>
                                        </h3>
                                        <div class="body-font textLight animate__animated fadeup">
                                            <?= get_sub_field('service_inclusion_desc'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if(get_field('show_bottom_box') && have_rows('bottom_boxes')): ?>
<section class="sec-padding single-service-clipped">
    <div class="container">
        <?php while(have_rows('bottom_boxes')):the_row(); ?>
        <div class="row align-items-center mb-4">
            <div class="col-lg-6 mb-4 mb-lg-0">
                <div class="img-clipbox">
                    <img src="<?= wp_get_attachment_image_src(get_sub_field('service_inclusion_image_b'),'full')[0]; ?>" alt="" class="img-fluid animate__animated fadeup"/>
                </div>
            </div>
            <div class="col-lg-6 pl-lg-5">
                <h2 class="sec-heading text-white text-uppercase mb-4 animate__animated fadeup"><?= get_sub_field('bottom_box_title_b'); ?></h2>
                <div class="body-font text-white animate__animated fadeup">
                    <?= get_sub_field('bottom_box_description_b'); ?>
                </div>
            </div>                
        </div>
        <?php if(get_sub_field('large_text') == 'Yes'): ?>
            <div class="row mb-4">
                <?php if(get_sub_field('number_of_columns_require') == 1): ?>
                    <div class="col-lg-12">
                        <div class="body-font text-white animate__animated fadeup">
                            <?= get_sub_field('column_1'); ?>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="col-lg-6 mb-4 mb-lg-0">
                        <div class="body-font text-white animate__animated fadeup">
                            <?= get_sub_field('column_1'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="body-font text-white animate__animated fadeup">
                            <?= get_sub_field('column_2'); ?>
                        </div>
                    </div>
                <?php endif; ?>                
            </div>
        <?php endif; ?>
        <?php endwhile; ?>
    </div>
</section>
<?php endif; ?>
<section class="call-to-action faq sec-padding">
    <div class="container">
        <div class="row">
        <div class="col-md-4 col-lg-5 mb-4 mb-md-0 pr-lg-5">
            <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
            <?= do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
        </div>
        <div class="col-md-8 offset-lg-1 col-lg-6 pl-md-4">
            <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked Questions</span></h2>
            <div class="accordion" id="accordionExample">
                <?php $j=1; while(have_rows('service_faqs')):the_row(); ?>
                <div class="card">
                    <div class="card-header" id="heading<?= $j; ?>">
                    <h2 class="mb-0 <?= ($j > 1 ? 'collapsed' : ''); ?>" data-toggle="collapse" data-target="#collapse<?= $j; ?>" aria-expanded="true" aria-controls="collapse<?= $j; ?>">
                        <div class="row align-items-center">
                        <div class="col-10">
                            <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0"><?= get_sub_field('question'); ?></p>
                        </div>
                        <div class="col-2 text-center">
                            <div class="accordion-plus">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="color: rgb(0, 198, 255); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 36" class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false"><path d="M34 18A16 16 0 1 1 18 2a16 16 0 0 1 16 16zm-8.41-1.5H19.5v-6.09a1.5 1.5 0 0 0-3 0v6.09h-6.09a1.5 1.5 0 0 0 0 3h6.09v6.09a1.5 1.5 0 0 0 3 0V19.5h6.09a1.5 1.5 0 0 0 0-3z" class="clr-i-solid clr-i-solid-path-1" fill="currentColor"></path></svg>
                            </div>
                            <div class="accordion-minus">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" focusable="false" width="1em" height="1em" style="color: rgb(0, 198, 255); transform: rotate(360deg);" preserveAspectRatio="xMidYMid meet" viewBox="0 0 36 36" class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false"><path d="M18 2a16 16 0 1 0 16 16A16 16 0 0 0 18 2zm6 17.5H12a1.5 1.5 0 0 1 0-3h12a1.5 1.5 0 0 1 0 3z" class="clr-i-solid clr-i-solid-path-1" fill="currentColor"></path></svg>
                            </div>
                        </div>
                        </div>
                    </h2>
                    </div>
                    <div id="collapse<?= $j; ?>" class="collapse <?= ($j == 1 ? 'show' : ''); ?>" aria-labelledby="heading<?= $j; ?>" data-parent="#accordionExample">
                        <div class="body-font textLight card-body font-size-small">
                            <?= get_sub_field('answer'); ?>
                        </div>
                    </div>
                </div>
                <?php $j++; endwhile; ?>
            </div>
        </div>
        </div>
    </div>
    </section>
<?php endwhile; get_footer(); ?>