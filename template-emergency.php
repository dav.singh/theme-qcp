<?php
/*
Template Name: Emergency
*/
get_header(); ?>

<section class="hero qcp position-relative">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h1 class="text-uppercase text-white mb-4"><span class="movingletters">Emergency Plumber</span><br/><span class="movingletters">Gold Coast 24X7</span></h1>
                    <p class="text-white mb-4 animate__animated fadeup">Whether it’s a burst water pipe, blocked toilet, no hot water….NO PROBLEM our team of qualified plumbers are waiting for you to call us</p>
                    <a href="tel:4181-189-032" class="btn btn-primary danger animate__animated fadeup">
                        <div class="d-flex align-items-center">
                            <span class="text-uppercase mr-3">Emergency Call 4181-189-032</span>
                            <div class="iconfontsize">
                                <span class="iconify" data-icon="carbon:chevron-right" data-inline="false"></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="qcp-title sec-padding position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6  mb-4 mb-md-0">
                    <p class="textDark font-family-body font-bold font-size-24 text-center text-md-left animate__animated animate__bounceInLeft">Emergency Plumbing Services Logan to Gold Coast to Tweed Heads</p>
                </div>
                <div class="col-md-6 mb-4 mb-md-0">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid d-block d-md-none animate__animated fadeup" />
                </div>
            </div>
        </div>
        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid qcp-reading-floating d-none d-md-block animate__animated animate__bounceInRight" />
    </section>
    <section class="what-we-do sec-padding no-bg-image">
        <section class="upper-blue-cut position-relative emergency">
            <div class="container">
                <div class="row frontlayer">
                  <div class="col-md-6 offset-md-6">
                    <h2 class="textDark headingfont sec-heading mb-4 text-uppercase animate__animated fadeup">Do you have a plumbing emergency?</h2>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">Our plumbers are here to help you, 24 hours a day, 7 days a week.</p>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">Our Gold Coast plumbers provide a wide range of plumbing services such as:</p>
                    <ul class="">
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Tap leak repair</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Temporary and replacement hot water services</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Burst pipe repair</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Leaky toilet repair</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Blocked drains</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Plumbing repairs</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Broken water heaters</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Clogged drains</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Low water pressure repair</li>
                      <li class="bodyfont textLight font-size-smallest animate__animated fadeup">and any other emergency plumbing issue that you may encounter</li>
                    </ul>
                    <p class="bodyfont textLight font-size-smallest animate__animated fadeup">Plumbing emergencies require you to act fast. That's why we're here for YOU. We don't want you wasting your valuable time searching after hours for emergency plumbers in the Gold Coast area that don't charge extra fees for urgent response to plumbing emergencies.</p>
                  </div>
                </div>
              </div>
        </section>
        <section class="position-relative building-full">
            <div class="container">
                <div class="row frontlayer">
                    <div class="col-md-6 mb-4 bodyfont textLight font-size-smallest text-white-phone">
                      <h2 class="textDark headingfont sec-heading mb-4 text-uppercase animate__animated fadeup">
                        What do our plumbers do?
                      </h2>
                      <ul class="">
                        <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Our emergency plumbers will arrive at your property as soon as possible, thus preventing further water damage and ensuring your system is working again.</li>
                        <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Our phone triage plumber is here to give you advice, thus preventing further damage until our emergency plumber can arrive.</li>
                        <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Our emergency plumbers take the time to understand the intricacies of each plumbing system to best determine what the problem is and how it can be solved.</li>
                        <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Our plumbers also do preventive maintenance work, thereby catching any potential problems before they become bigger issues.</li>
                        <li class="bodyfont textLight font-size-smallest animate__animated fadeup">Our plumbers ensure that our customers know how to keep their plumbing system safe and working at its finest both now and in the future.</li>
                      </ul>
                      <p class="bodyfont textLight font-size-smallest animate__animated fadeup">
                        Contact us, your emergency Gold Coast Plumber, now at <a href="tel:1800 367 727">1800 367 727</a>.
                      </p>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/building-full.jpg" class="img-fluid d-block d-md-none animate__animated fadeup" />          
                    </div>
                  </div>
              </div>
              <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/building-full.jpg" class="floating-building d-none d-md-block animate__animated animate__bounceInRight" />
        </section>
      </section>
      <section class="our-partners sec-padding">
          <div class="container">
              <div class="row">
                  <div class="col">
                      <h2 class="sec-heading color-sky text-uppercase text-center mb-4 mb-md-5"><span class="movingletters">Our construction partners</span></h2>
                  </div>
              </div>
              <div class="row">
                  <div class="col">
                      <div class="partner-slider">
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-1.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-2.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-3.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-4.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-5.png" alt="" /></div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked Questions</span></h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">How To Deal With A Plumbing Emergency?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                        It is easy to be caught stunned during plumbing emergencies, and sometimes mistakes are made in the early part of the event. If you want to know what to do when you need an emergency plumber in Gold Coast, QLD Coastal Plumbing has you covered to prevent the plumbing problem from getting out of hand.</div>
                    </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">How To Turn Off Your Water Heater?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                        <p>For Gas Water Heaters: Turn the knob on top of the thermostat clockwise to the off position. The thermostat is located near the bottom of the water heater.</p>
                        <p>For Electric Water Heaters: Go to your circuit breaker panel, find the breaker for your water heater, and switch it to the off position.</p>
                        <p>Turn The Water Supply Off</p>
                        <p>Twist the handle on the water supply shut off valve clockwise until it stops. This valve is typically located on top of the water heater, on the right side cold line.</p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">How do I know if I have a gas leak?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                        <p>Do you have a gas leak? Natural gas is scent free, therefore the manufacturer adds a special chemical to the natural gas in order to aid you in being aware of possible gas leaks.</p>
                        <p>If you suddenly smell “rotten eggs” you probably have a gas leak. If you are by the fixture that uses gas, ensure the pilot light is lit. If it is lit, you can check the lines for leaks by spraying with a mild soap solution to determine the location of the leak. The area that is leaking will bubble.  Turn off the gas supply by turning it clockwise.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<?php get_footer(); ?>      