<?php
/*
Template Name: Maintenance
*/
get_header(); ?>

<section class="hero qcp position-relative">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 offset-lg-3 text-center">
                    <h1 class="text-uppercase text-white mb-4"><span class="movingletters">Residential Plumbers</span><br/><span class="movingletters">Gold Coast Queensland</span></h1>
                    <p class="text-white mb-4 animate__animated fadeup">Our team of skilled plumbers will offer a complete plumbing maintenance solution. Servicing all areas from Logan to Tweed Heads South since 2008.</p>
                    <a href="<?= get_permalink(16); ?>" class="btn btn-primary animate__animated fadeup">
                        <div class="d-flex align-items-center">
                            <span class="text-uppercase mr-3">Learn more</span>
                            <div class="iconfontsize">
                                <span class="iconify" data-icon="carbon:chevron-right" data-inline="false"></span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="qcp-title sec-padding position-relative">
        <div class="container">
            <div class="row">
                <div class="col-md-6  mb-4 mb-md-0">
                    <p class="textDark font-family-body font-bold font-size-24 text-center text-md-left animate__animated animate__bounceInLeft">For comprehensive plumbing maintenance, call the experts at
                        Queensland Coastal Plumbing today!</p>
                </div>
                <div class="col-md-6 mb-4 mb-md-0">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid d-block d-md-none animate__animated fadeup" />
                </div>
            </div>
        </div>
        <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/reader-full.jpg" class="img-fluid qcp-reading-floating d-none d-md-block animate__animated animate__bounceInRight" />
    </section>
    <section class="what-we-do sec-padding no-bg-image">
        <section class="upper-blue-cut position-relative">
            <div class="container">
                <div class="row frontlayer">
                  <div class="col-md-6 offset-md-6">
                    <h2 class="textDark headingfont sec-heading mb-4 text-uppercase"><span class="movingletters">Complete Plumbing Solutions</span></h2>
                    <div class="bodyfont textLight font-size-smallest">
                        <p class="animate__animated fadeup">With years of experience, predominantly in the commercial sector, Qld Coastal Plumbing is well-prepared to handle any residential plumbing projects, from plumbing emergencies to remodelling to new builds. Our goal is to complete each project quickly to ensure minimal disruption and a speedy return to normalcy.</p>
                        <p class="animate__animated fadeup">Our newly-formed Maintenance Service Division strives to deliver an old-fashioned plumbing service. Our residential plumbers will be on time, well-presented, and communicate with our clients throughout the job.</p>
                        <p class="animate__animated fadeup">With the use of the latest equipment, we deliver our residential customers a stellar service at a fair price.</p>
                        <p class="animate__animated fadeup">QLD Coastal Plumbing assure you an experienced and professional quality service at every turn. Our zero-tolerance approach to safety risks sets us apart as an industry leader in safe work practices.</p>
                        <p class="animate__animated fadeup">We assure our customers a high quality finish every time. Our consistency and speedy service will ensure you can rely on us whenever you need plumbing assistance. Whether you have a clogged drain, hot water systems, or water issue, we're here to help you.</p>
                    </div>
                  </div>
                </div>
              </div>
        </section>
        <section class="diagonal psudeo-diagonal sec-padding">
          <div class="container">
            <div class="row">        
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(72); ?>">
                          <img src="<?= wp_get_attachment_url(76); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(72); ?>">Blocked Drains</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can clear any blockage from 40mm to 225mm pipe. We fully guarantee our work with a camera report at completion.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(77); ?>">
                          <img src="<?= wp_get_attachment_url(79); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(77); ?>">Hot water systems</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can replace or repair your hot water system. With quality workmanship and same day service.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(88); ?>">
                          <img src="<?= wp_get_attachment_url(89); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(88); ?>">Leak detection / Burst pipes</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can pin point your leak using the latest equipment then repair and re-establish with minimum disruption to the client.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(92); ?>">
                          <img src="<?= wp_get_attachment_url(93); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(92); ?>">Toilet and tap repairs</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can repair or replace any fixture in your home or commercial property. We also do water efficiency reports.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(96); ?>">
                          <img src="<?= wp_get_attachment_url(98); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(96); ?>">Gas fitting</a>
                          </h3>
                          <p class="body-font textLight text-center">Our gas plumbers can repair and service most appliances in a safe manner. We can also install a new gas service to any property.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                        <a href="<?= get_permalink(101); ?>">
                          <img src="<?= wp_get_attachment_url(103); ?>" class="w-100 img-fluid"/>
                        </a>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(101); ?>">Backflow protection</a>
                          </h3>
                          <p class="body-font textLight text-center">We can test and service your old backflow valves yearly or install new valves to comply with the local authority.</p>
                      </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4 pr-md-0">
                    <div class="serv-thumb h-100" style="background-image: url(<?= wp_get_attachment_url(135); ?>);background-repeat: no-repeat;background-position:center;background-size:cover">
                      <img src="<?= wp_get_attachment_url(135); ?>" class="w-100 img-fluid invisible"/>
                    </div>
                </div>
                <div class="col-md-8 pl-md-0">
                    <div class="servicebar p-4 pl-md-5 pr-md-5 keepsameheight">
                        <h3 class="textDark mb-3 text-capitalize cardHeading animate__animated fadeup">
                          Are there plumbers near me?
                        </h3>
                        <div class="body-font textLight animate__animated fadeup font-size-smallest">
                            <p class="animate_animated fadeup font-size-smallest pb-0">Yes. </p>
                            <p class="animate_animated fadeup font-size-smallest pb-0">Our team employs local tradespeople to provide professional plumbing services to homeowners throughout the Gold Coast.</p>
                            <p class="animate_animated fadeup font-size-smallest pb-0">Few things are more frustrating than a home plumbing emergency, so we are here to provide assistance in a few simple ways:</p>
                            <ol>
                              <li class="animate_animated fadeup">We do not have any call out fees and we will only charge from the time we arrive on the job.</li>
                              <li class="animate_animated fadeup">Our Rapid Response team is available 24/7 for emergency plumbing services throughout the Gold Coast.</li>
                              <li class="animate_animated fadeup">Our technicians can give you advice over the phone when you call to prevent any further damage to your property. This will hopefully prevent further water damage or house damage from occurring before our residential plumbers arrive.</li>
                              <li class="animate_animated fadeup">We can save you money, time and inconvenience by protecting your assets from unplanned breakdowns and costly emergency services.</li>
                              <li class="animate_animated fadeup">We also provide preventative upkeep of your plumbing system, which will ensure your system is working properly all the time.</li>
                            </ol>
                            <h4 class="textDark">Qld Coastal Plumbing specialises in:</h4>
                            <ol>
                              <li class="animate_animated fadeup">Blocked Drains</li>
                              <li class="animate_animated fadeup">Residential Plumbing Emergencies</li>
                              <li class="animate_animated fadeup">Hot water heater repair and servicing</li>
                              <li class="animate_animated fadeup">Leak detection, burst pipes, and water leaks</li>
                              <li class="animate_animated fadeup">Gas fitting</li>
                              <li class="animate_animated fadeup">Backflow protection and pipe repairs</li>
                              <li class="animate_animated fadeup">Yearly preventative maintenance to prevent further emergencies</li>
                            </ol>
                        </div>
                    </div>
                </div>
              </div>
          </div>
        </section>
        <section class="position-relative">
          <div class="container">
            <div class="row frontlayer">
                <div class="col-md-6 mb-4">
                  <h2 class="textDark headingfont sec-heading mb-4 text-uppercase animate__animated fadeup">Emergency Plumbing Services</h2>
                  <p class="body-font textLight font-size-smallest animate__animated fadeup">Are you in need of an emergency plumber? Call us now at 1800 367 727 and our emergency Rapid Response team is available 24/7 to offer assistance. We do not have any call-out fees and our charges will only begin once we’ve arrived on the job. We even triage your plumbing problems over the phone, to prevent any further damage to your system. </p>
                  <p class="body-font textLight font-size-smallest animate__animated fadeup">Here at Queensland Coastal Plumbing, we’ve seen it all and we’ve solved it all. From complex multi residential to cost-effective maintenance jobs, our extensive services can help you complete your job on time!</p>
                  <a href="<?= get_permalink(16); ?>" class="btn btn-primary text-uppercase mt-4 animate__animated fadeup">Book a service</a>
                </div>
              </div>
          </div>
        </section>
      </section>
      <section class="our-partners sec-padding position-relative">
          <div class="container">
              <div class="row">
                  <div class="col">
                      <h2 class="sec-heading color-sky text-uppercase text-center mb-4 mb-md-5"><span class="movingletters">Our construction partners</span></h2>
                  </div>
              </div>
              <div class="row">
                  <div class="col">
                      <div class="partner-slider">
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-1.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-2.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-3.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-4.png" alt="" /></div>
                          <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-5.png" alt="" /></div>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked</span></h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provides maintenance and repair services and more to residents from South Brisbane, throughout the Gold Coast to Tweed South and surrounding areas. 
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my QLD plumber?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <ul>
                        <li>Professional and prompt service</li>
                        <li>No call-out fees </li>
                        <li>Competitive rates </li>
                        <li>Stellar service</li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      We have a “no call-out fee” service which guarantees you only pay for the time we’re on the job. 
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul class="">
                        <li>Blocked drains repair</li>
                        <li>Hot water heater repair and servicing</li>
                        <li>Leak detection, burst pipes, and water leaks</li>
                        <li>Gas fitting</li>
                        <li>Backflow protection and pipe repairs </li>
                        <li>Yearly preventative maintenance</li>
                        <li>Installation of toilets, taps, and other plumbing parts</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

<?php get_footer(); ?>      