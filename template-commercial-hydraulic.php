<?php 
/*
Template Name: Commerical Industrial Hydraulic services
*/
get_header(); ?>
<section class="page-hero position-relative services-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-9 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5">Commercial & Industrial Hydraulic Services</h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="costbar pt-4 pb-4 position-relative">
            <div class="container">
                <div class="row align-items-center text-center text-md-left">
                    <div class="col-md-8">
                        <h3 class="body-font animate__animated fadeup">Providing well-managed, cost-effective and timely commercial and industrial Hydraulic plumbing services to suit your project and schedule.</h3>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="sec-padding pb-3">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <h2 class="textDark text-uppercase mb-3 mb-md-4 text-center sec-heading">Cost Effective Commercial Hyrdraulic Plumbing Services</h2>
                        <div class="textLight body-font font-size-smallest mb-md-4 text-center">
                            <p class="animate__animated fadeup"><strong>Providing well-managed, cost-effective and timely commercial and industrial  hydraulic plumbing services to suite your project and schedule.</strong></p>
                            <p class="animate__animated fadeup">Since 2008, QCP has successfully completed Hydraulic plumbing services for industrial and commercial projects ranging from Aged Care and healthcare facilities to large, challenging renovations and new commercial constructions. QCP can take care of a range of commercial and industrial jobs, including:</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
      <div class="row mt-4 mb-4">
          <div class="col-md-10 offset-md-1">
            <div class="row">
              <div class="col-md-6 mb-4">
                <div class="row align-items-center animate__animated fadeup">
                  <div class="col-3 text-center">
                      <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color:#00c6ff;" data-width="60"></span>
                  </div>
                  <div class="col-9 pl-md-4">
                      <p class="body-font font-bold font-size-regular mb-0">Hydraulic plumbing services</p>
                  </div>
                </div>
              </div>
              <div class="col-md-6 mb-4">
                <div class="row align-items-center animate__animated fadeup">
                    <div class="col-3 text-center">
                        <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color:#00c6ff;" data-width="60"></span>
                    </div>
                    <div class="col-9 pl-md-4">
                        <p class="body-font font-bold font-size-regular mb-0">Fire and gas</p>
                    </div>
                </div>
              </div>
              <div class="col-md-6 mb-4">
                <div class="row align-items-center animate__animated fadeup">
                    <div class="col-3 text-center">
                        <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color:#00c6ff;" data-width="60"></span>
                    </div>
                    <div class="col-9 pl-md-4">
                        <p class="body-font font-bold font-size-regular mb-0">Civil works</p>
                    </div>
                </div>
              </div>
              <div class="col-md-6 mb-4">
                <div class="row align-items-center animate__animated fadeup">
                    <div class="col-3 text-center">
                        <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color:#00c6ff;" data-width="60"></span>
                    </div>
                    <div class="col-9 pl-md-4">
                        <p class="body-font font-bold font-size-regular mb-0">Civil infrastructure</p>
                    </div>
                </div>
              </div>  
            </div>
          </div>  
      </div>
      <div class="row mt-4 mb-4">
          <div class="col-12 body-font ">
              <p class="animate__animated fadeup">QCP employees are fully inducted by Gold Coast Water to provide hydraulic services and civil drain laying including pits, manholes, GPT’s, bio-retention and all civil needs to meet your project requirements.</p>
              <p class="animate__animated fadeup">Our Operations and Project Management Teams use an integrated process to lead and drive the project to ensure: completion of projects according to specified quality, performance, schedule requirements, keeping all works within budget, maintaining profitability. QCP place a strong focus on employee safety as well as the safety of all stakeholders involved.</p>
              <p class="animate__animated fadeup">We are committed to make it easy for our clients to do business with us.</p>
              <p class="animate__animated fadeup">With a highly efficient and seamless project delivery system and a single point of contact we are designed to get the project completed with minimal disruption to your business operations.</p>
          </div>
      </div>
    </div>
</section>
<section class="our-partners sec-padding">
    <div class="container">
        <div class="row">
            <div class="col">
                <h2 class="sec-heading color-sky text-uppercase text-center mb-4 mb-md-5"><span class="movingletters">Our construction partners</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="partner-slider">
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-1.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-2.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-3.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-4.png" alt="" /></div>
                    <div><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/partner-5.png" alt="" /></div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="call-to-action faq sec-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-lg-6 mb-4 mb-md-0">
              <h2 class="mb-4 text-white sec-heading"><span class="movingletters">Get in touch</span></h2>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">call us</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:1800 367 727">1800 367 727</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Emergency/After Hours Hotline</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><a href="tel:0481 189 032">0481 189 032</a></p>
              </div>
              <div class="contact-item mb-4">
                <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">address</p>
                <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup">4/14 Rothcote Court Burleigh<br/>Heads QLD 4220</p>
              </div>
            </div>
            <div class="col-md-8 col-lg-6">
              <h2 class="mb-4 fq-dark sec-heading"><span class="movingletters">Frequently Asked</span></h2>
              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What Areas Do You Service?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      QLD Coastal Plumbing provide services to local residents and commercial properties from South Brisbane, throughout the Gold Coasts to Tweed South and surrounding areas.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="row align-items-center">
                        <div class="col-10">
                        <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What can I expect from my plumber on the Gold Coast?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      No call out fees and competitive rates
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What are your call out fees?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                    With our “no call out fee” service guarantee you only pay for time engaged on your job, saving money for local residents all along the Gold Coast.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingFour">
                    <h2 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="row align-items-center">
                        <div class="col-10">
                          <p class="font-family-body font-size-regular textDark text-capitalize font-bold mb-0">What services do you offer?</p>
                        </div>
                        <div class="col-2 text-center">
                          <div class="accordion-plus">
                            <span class="iconify" data-icon="clarity:plus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                          <div class="accordion-minus">
                          <span class="iconify" data-icon="clarity:minus-circle-solid" data-inline="false" style="color: #00c6ff;"></span>
                          </div>
                        </div>
                      </div>
                    </h2>
                  </div>
                  <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                    <div class="body-font textLight card-body font-size-small">
                      <p>Qld Coastal Plumbing offer everything plumbing</p>
                      <ul class="pl-0 list-unstyled">
                        <li><b><u>P</u></b>rofessional and progressive advice</li>
                        <li><b><u>L</u></b>egislative compliance</li>
                        <li><b><u>U</u></b>nwavering adherence to quality and service</li>
                        <li><b><u>M</u></b>arket leading rates</li>
                        <li><b><u>B</u></b>ig coverage and serviceability</li>
                        <li><b><u>I</u></b>nnovative solutions saving you time and money</li>
                        <li><b><u>N</u></b>o harm safety policy</li>
                        <li><b><u>G</u></b>old class solutions every time</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
<?php get_footer(); ?>