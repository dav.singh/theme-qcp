<?php
/*
Template Name: Services
*/
get_header(); ?>

<section class="page-hero position-relative services-hero">
        <div class="container first">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mt-5">
                    <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><span class="movingletters">Services</span></h1>
                </div>
            </div>
        </div>
        <div class="page-hero-titled sec-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h2 class="textDark text-uppercase mb-3 mb-md-4 text-center sec-heading animate__animated fadeup">Residential & Commercial Plumbing Maintenance</h2>
                        <p class="textLight body-font font-size-smallest mb-md-4 text-center animate__animated fadeup">Our team of skilled plumbers will offer a complete plumbing maintenance solution. Servicing all areas from Logan to Tweed Heads South since 2008.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="parted-sec psudeo">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pr-lg-5">
                    <div class="sec-padding">
                        <div class="row">
                            <div class="col-lg-4">
                                <h2 class="sec-heading text-white text-uppercase">
                                    <span class="movingletters">Get</span> a <span class="movingletters">quick</span> <span class="movingletters">Gold</span> <span class="movingletters">Coast</span> <span class="movingletters">Quote</span> <span class="movingletters">Now</span>
                                </h2>
                                <div class="sep mt-4 mb-4">
                                    <div class="sep-line parted-fur animate__animated bounceInLeft"></div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <?= do_shortcode('[gravityform id="2" title="false" description="false" ajax="true"]'); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="pl-md-5 pr-md-5 col-lg-5">
                    <div class="right-part">
                        <div class="sec-padding">
                            <h2 class="sec-heading textDark text-center text-lg-left text-uppercase mb-5"><span class="movingletters">Trusted burleigh</span><br/><span class="movingletters">heads plumbers</span></h2>
                            <ul class="feature-checklist pl-0 list-unstyled">
                                <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                    <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                    <p class="body-font font-size-regular mb-0 ml-4">All Workmanship<br/>100% Guaranteed</p>
                                </li>
                                <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                    <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                    <p class="body-font font-size-regular mb-0 ml-4">Reliable Friendly<br/>Service</p>
                                </li>
                                <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                    <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                    <p class="body-font font-size-regular mb-0 ml-4">Well Presented –<br>On Time Every Time</p>
                                </li>
                                <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                    <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                    <p class="body-font font-size-regular mb-0 ml-4">Competitive Rates</p>
                                </li>
                            </ul>
                            <p class="d-block d-lg-none">&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-4">        
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(76); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(72); ?>">Blocked Drains</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can clear any blockage from 40mm to 225mm pipe. We fully guarantee our work with a camera report at completion.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(79); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(77); ?>">Hot water systems</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can replace or repair your hot water system. With quality workmanship and same day service.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(89); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(88); ?>">Leak detection / Burst pipes</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can pin point your leak using the latest equipment then repair and re-establish with minimum disruption to the client.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(93); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(92); ?>">Toilet and tap repairs</a>
                          </h3>
                          <p class="body-font textLight text-center">Our plumbers can repair or replace any fixture in your home or commercial property. We also do water efficiency reports.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(98); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(96); ?>">Gas fitting</a>
                          </h3>
                          <p class="body-font textLight text-center">Our gas plumbers can repair and service most appliances in a safe manner. We can also install a new gas service to any property.</p>
                      </div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-4 mb-4">
                  <div class="cardbox animate__animated">
                      <div class="cardThumb">
                          <img src="<?= wp_get_attachment_url(103); ?>" class="w-100 img-fluid"/>
                      </div>
                      <div class="cardBody p-3 bg-white keepsameheight">
                          <h3 class="textDark mb-4 text-center text-capitalize cardHeading">
                            <a href="<?= get_permalink(101); ?>">Backflow protection</a>
                          </h3>
                          <p class="body-font textLight text-center">We can test and service your old backflow valves yearly or install new valves to comply with the local authority.</p>
                      </div>
                  </div>
                </div>
              </div>
        </div>
    </section>
    <section class="sec-padding">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="sec-heading textDark text-uppercase mb-4 mb-md-5 text-center"><span class="movingletters">Servicing all major brands</span></h2>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <ul class="d-flex align-items-center flex-wrap brandlist pl-0">
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/AquaMax.jpg" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Bosch.png" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Mitsubishi.png" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Panasonic.png" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Carrier.png" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Dux.jpg" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Rheem.jpg" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Rinnai.jpg" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/LG.png" alt="" class="img-fluid"></li>
                        <li class="mb-4 mb-md-5"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/Vulcan.jpg" alt="" class="img-fluid"></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>    