<?php
/*
Template Name: About
*/
get_header();
?>

<section class="page-hero position-relative about-hero">
        <div class="container first">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mt-5">
                    <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><span class="movingletters">About us</span></h1>
                </div>
            </div>
        </div>
        <div class="page-hero-titled sec-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h2 class="font-bold textDark text-uppercase mb-3 mb-md-4 text-center sec-heading"><span class="movingletters">About us</span></h2>
                        <p class="textLight body-font font-size-smallest mb-md-4 text-center animate__animated fadeup">At Qld Coastal plumbing we want to have relationships with our clients.</p>
                        <p class="textLight body-font font-size-smallest mb-md-4 text-center animate__animated fadeup">We want your work forever, not just for one job. We hope to achieve this by providing well-presented plumbers that arrive on time and complete the job with minimal impact to our residential clients. We’re here for your planned plumbing projects as well as your plumbing emergencies.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="parted-sec">
        <div class="container">
            <div class="row">
                <div class="col-lg-7 pr-lg-5">
                    <div class="sec-padding">
                        <h2 class="sec-heading text-white text-uppercase animate__animated fadeup">A focus on providing a  service at a fair price and standing by our workmanship 100% of the time.</h2>
                        <div class="sep mt-4 mb-4">
                            <div class="sep-line"></div>
                        </div>
                        <p class="text-white body-font font-size-smallest mb-4 text-left animate__animated fadeup">Qld Coastal Plumbing is a company based on the belief that doing things right the first time, going the extra mile for a customer, and giving our best every single day will pay off with job satisfaction – for us and all those we help with plumbing problems.</p>
                        <p class="text-white body-font font-size-smallest mb-4 text-left animate__animated fadeup">We’re proud to say it’s working, so we’ll never stop delivering the same stellar service and straightforward pricing that’s gotten us this far.</p>
                        <p class="text-white body-font font-size-smallest mb-4 text-left animate__animated fadeup">We feel so good about what we do and who we work for, we offer some of the best prices in the Gold Coast area while never cutting corners in material or effort.</p>
                        <p class="text-white body-font font-size-smallest mb-4 text-left animate__animated fadeup">If you’re in need of a professional, residential plumber, call us today. We have plumbing experts ready to help you, no matter the time of day.</p>
                    </div>
                </div>
                <div class="pl-md-5 pr-md-5 col-lg-5 right-part">
                    <div class="sec-padding">
                        <h2 class="sec-heading textDark text-uppercase mb-5 text-center text-lg-left"><span class="movingletters">Trusted burleigh</span><br/><span class="movingletters">heads plumbers</span></h2>
                        <ul class="feature-checklist pl-0 list-unstyled">
                            <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                <p class="body-font font-bold font-size-regular mb-0 ml-4">All Workmanship<br/>100% Guaranteed</p>
                            </li>
                            <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                <p class="body-font font-bold font-size-regular mb-0 ml-4">Reliable Friendly<br/>Service</p>
                            </li>
                            <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                <p class="body-font font-bold font-size-regular mb-0 ml-4">Well Presented –<br>On Time Every Time</p>
                            </li>
                            <li class="feature-checklist-item d-flex align-items-center mb-4 animate__animated fadeup">
                                <span class="iconify" data-icon="cil:check-circle" data-inline="false" style="color: #00c6ff;" data-width="45"></span>
                                <p class="body-font font-bold font-size-regular mb-0 ml-4">Competitive Rates</p>
                            </li>
                        </ul>
                        <p class="d-block d-lg-none">&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>    