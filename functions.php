<?php

require_once "inc/wp-bootstrap-navwalker.php";
add_theme_support('post-thumbnails');
if ( ! function_exists( 'qcp_register_nav_menu' ) ) { 
    function qcp_register_nav_menu(){
        register_nav_menus( array(
            'main_menu' => __( 'Main Menu', 'qcp' )
        ) );
    }
    add_action( 'after_setup_theme', 'qcp_register_nav_menu', 0 );
}

/**
 * Add login logout menu item in the main menu.
 * ===========================================
 */

add_filter( 'wp_nav_menu_items', 'lunchbox_add_loginout_link', 10, 2 );
function lunchbox_add_loginout_link( $items, $args ) {
    $items .= '<li class="nav-item phone">
                    <a href="tel:1800 367 727" class="nav-link phone-link d-flex align-items-center">
                        <span class="phone-icon-span animate__animated">
                            <span class="iconify" data-icon="fa-solid:phone" data-inline="false"></span>
                        </span>
                        <span class="ml-3">1800 367 727</span>
                    </a>
                </li>';
    return $items;
}

add_image_size( 'news-thumb', 600, 400, array( 'left', 'top' ) );

add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {
    return "<button class='button gform_button btn btn-primary long' id='gform_submit_button_{$form['id']}'>Send</button>";
}

// Force Gravity Forms to init scripts in the footer and ensure that the DOM is loaded before scripts are executed.
add_filter( 'gform_init_scripts_footer', '__return_true' );
add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open', 1 );
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close', 99 );

function wrap_gform_cdata_open( $content = '' ) {
	if ( ! do_wrap_gform_cdata() ) {
		return $content;
	}
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ' . $content;
	return $content;
}

function wrap_gform_cdata_close( $content = '' ) {
	if ( ! do_wrap_gform_cdata() ) {
		return $content;
	}
	$content .= ' }, false );';
	return $content;
}

function do_wrap_gform_cdata() {
	if (
		is_admin()
		|| ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		|| isset( $_POST['gform_ajax'] )
		|| isset( $_GET['gf_page'] ) // Admin page (eg. form preview).
		|| doing_action( 'wp_footer' )
		|| did_action( 'wp_footer' )
	) {
		return false;
	}
	return true;
}

remove_filter( 'the_category', 'no_links' );
function no_links($thelist) {
	return preg_replace('#<a.*?>([^<]*)</a>#i', '$1', $thelist);
}   
add_filter( 'the_category', 'no_links' );