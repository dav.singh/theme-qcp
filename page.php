<?php get_header(); while(have_posts()):the_post(); ?>
<section class="page-hero position-relative services-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5"><?= get_the_title(); ?></h1>
            </div>
        </div>
    </div>
    <!-- <div class="page-hero-titled halfsec-padding">
        
    </div> -->
</section>
<section class="pt-4 pt-md-5 pb-4 pb-md-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12 body-font font-size-regular body-font">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<?php endwhile; get_footer(); ?>