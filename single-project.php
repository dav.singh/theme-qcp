<?php get_header(); while(have_posts()):the_post(); ?>
<section class="page-hero position-relative contact-hero">
    <div class="container first">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-5">
                <h1 class="text-uppercase text-white mb-4 sec-heading font-size-hero mt-5 animate__animated fadeup"><?= get_the_title(); ?></h1>
            </div>
        </div>
    </div>
    <div class="page-hero-titled half">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-4 pt-5 pb-5 contact-clipped-bg ninty">
                    <div class="pl-3 pr-3 pb-5 pb-xl-0">
                        <h2 class="text-white text-uppercase mb-3 mb-md-4 text-center text-md-left sec-heading mt-4 animate__animated fadeup">Project Info</span></h2>    
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup"><?= get_field('project_description'); ?></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Client</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><?= get_field('client'); ?></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">Categories</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><?= get_field('categories'); ?></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">location</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><?= get_field('project_address'); ?></p>
                        </div>
                        <div class="contact-item mb-4">
                            <p class="text-uppercase font-light font-size-small mb-1 text-white font-family-body animate__animated fadeup">value</p>
                            <p class="mb-0 text-white font-size-large font-family-body font-bold animate__animated fadeup"><?= get_field('project_value'); ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 pt-5 pb-5 pl-md-5">
                    <img src="<?= wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full')[0]; ?>" alt="" class="img-fluid mx-auto d-table mb-md-5" />
                </div>
            </div>
        </div>
    </div>
</section>
<?php endwhile; ?>
<?php get_footer(); ?>    